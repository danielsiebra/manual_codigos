class PlanneresController < ApplicationController
  before_action :set_planner, only: [:show, :edit, :update, :destroy]

  # GET /planneres
  # GET /planneres.json
  def tarefas
    @date = params[:month] ? Date.parse(params[:month]) : Date.today
    @planneres = Planner.where(:ativo => true, :empresa_id => session[:empresa_id]).order(:titulo)
    respond_to do |format|
      format.html
    end 
  end
  
  # GET /planneres/1
  # GET /planneres/1.json
  def detalhes
    @planner = Planner.find(params[:id])
  end

  def show
  end

  # GET /planneres/new
  def new
    @planner = Planner.new
  end

  # GET /planneres/1/edit
  def edit
  end

  # POST /planneres
  # POST /planneres.json
  def create
    @planner = Planner.new(planner_params)
    @planner.ativo = true
    @planner.empresa_id = session[:empresa_id]

    respond_to do |format|
      if @planner.save
        format.html { redirect_to @planner, notice: 'Tarefa cadastrada com sucesso.' }
        format.json { render action: 'show', status: :created, location: @planner }
      else
        format.html { render action: 'new' }
        format.json { render json: @planner.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /planneres/1
  # PATCH/PUT /planneres/1.json
  def update
    respond_to do |format|
      if @planner.update(planner_params)
        format.html { redirect_to @planner, notice: 'Planner was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @planner.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /planneres/1
  # DELETE /planneres/1.json
  def destroy
    @planner.update(ativo: false)
    respond_to do |format|
      format.html { redirect_to planneres_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_planner
      @planner = Planner.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def planner_params
      params.require(:planner).permit(:date, :titulo, :descricao, :periodo)
    end
end
