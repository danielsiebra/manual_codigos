class CreatePlanneres < ActiveRecord::Migration
  def change
    create_table :planneres do |t|
      t.date :date
      t.string :titulo
      t.text :descricao
      t.string :periodo
      t.boolean :ativo, default: true
      t.references :empresa, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
