json.array!(@planneres) do |planner|
  json.extract! planner, :id, :data, :titulo, :descricao, :periodo
  json.url planner_url(planner, format: :json)
end
