def filtrar_campo_data
	authorize(:nome_model, :funcao?)
	
	@data_inicial = params[:data_inicial]
	@data_final = params[:data_final]

	@nome_controller = NomeModel.where(:campo_data => [@data_inicial..@data_final]).order(:campo_data)

	render 'index'
end