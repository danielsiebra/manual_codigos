def cadastrar
    @verificar = NomeModel.find_by(:nome_campo => params[:nome_campo])
    if !@verificar.nil?
      @mensagem = "erro_uso"
      @anteriores = NomeModel.select(:nome_campo_select, :nome_campo).where("created_at >= '#{Date.today.to_time}'").order(created_at: :desc)
      render :inline => render_to_string(:partial => 'listagem')
    else
      if params[:nome_campo_select] == "" || params[:nome_campo]
        @mensagem = "erro"
        @anteriores = NomeModel.select(:nome_campo, :nome_campo_select).where("created_at >= '#{Date.today.to_time}'").order(created_at: :desc)
        render :inline => render_to_string(:partial => 'listagem')
      else
        @nome_controller = NomeModel.create(:empresa_id => session[:empresa_id], :ativo => true, :nome_campo_select, :nome_campo)

        @anteriores = NomeModel.select(:nome_campo, :nome_campo_select).where("created_at >= '#{Date.today.to_time}'").order(created_at: :desc)
        @mensagem = 'NomeModel cadastrado com sucesso.'
        render :inline => render_to_string(:partial => 'listagem')
      end
    end
end